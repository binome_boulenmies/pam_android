package com.example.sylvain.tp1_tinderlike;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import model.Results;
import model.User;

/**
 * Created by pomiesco on 02/02/16.
 */
public class DataManager {
    private static DataManager ourInstance = null;

    public static DataManager getInstance(Context context) {
        if (ourInstance == null) {
            synchronized (DataManager.class) {
                if (ourInstance == null) {
                    ourInstance = new DataManager(context);
                }
            }
        }

        return ourInstance;
    }

    private Results results;
    private RequestQueue queue;

    private List<User> usersLike;

    private DataManager(Context context) {
        results = new Results();
        usersLike = new ArrayList<User>();

        queue = Volley.newRequestQueue(context);

    }

    public User getCurrent() {
        return results.topUser();
    }

    public User getNext() {
        results.popUser();

//        if(results.getResults().size() < 10){
//            getData(new IDataListener() {
//                @Override
//                public void onDataReceive(User user) {
//                }
//
//                @Override
//                public void onError(String string) {
//
//                }
//            });
//        }
        return results.topUser();
    }

    public void likeUser(User user) {
        usersLike.add(user);
    }

    public void getData(final IDataListener listener) {
        // Instantiate the RequestQueue.
        String url ="https://randomuser.me/api/?format=json&results=3&nat=fr";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //briefDescription.setText("Response is: "+ response.substring(0,500));
                        Gson gson = new Gson();
                        results.addResults(gson.fromJson(response, Results.class));
                        listener.onDataReceive(getCurrent());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO log
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
