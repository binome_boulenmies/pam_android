package com.example.sylvain.tp1_tinderlike;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import model.User;

public class DetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView nameFirst;
    private TextView nameLast;
    private TextView gender;

    private DataManager dataManager;
    private User currentUser;

    private Button nopeBtn;
    private Button likeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        imageView = (ImageView) findViewById(R.id.userPhoto);
        nameFirst = (TextView) findViewById(R.id.nameFirst);
        nameLast = (TextView) findViewById(R.id.nameLast);
        gender = (TextView) findViewById(R.id.gender);

        nopeBtn = (Button) findViewById(R.id.nopeBtn);
        nopeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("isiTinder", "Nope button clicked");
                sayNope();
                displayUser(currentUser);
            }
        });

        likeBtn = (Button) findViewById(R.id.likeBtn);
        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("isiTinder", "Like button clicked");
                likeUser();
                displayUser(currentUser);
            }
        });

        dataManager = DataManager.getInstance(getApplicationContext());
        currentUser = dataManager.getCurrent();
        displayUser(currentUser);
    }

    public void sayNope (){ currentUser = dataManager.getNext(); }


    public void likeUser (){
        dataManager.likeUser(currentUser);
        currentUser = dataManager.getNext();
    }

    public void displayUser (User user){
        if (user != null) {
            Picasso.with(getApplicationContext()).load(user.getPicture().getMedium()).into(imageView);
            nameFirst.setText(user.getName().getFirst());
            nameLast.setText(user.getName().getLast());
            gender.setText(user.getGender());
        }
    }
}
