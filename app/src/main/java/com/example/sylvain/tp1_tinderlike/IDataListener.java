package com.example.sylvain.tp1_tinderlike;

import model.User;

/**
 * Created by pomiesco on 02/02/16.
 */
public interface IDataListener {
    public void onDataReceive(User user);
    public void onError(String string);
}
