package com.example.sylvain.tp1_tinderlike;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import model.Results;
import model.User;

public class isiTinder extends AppCompatActivity {

    private TextView briefDescription;
    private ImageView imageView;
    private DataManager dataManager;
    private Button nopeBtn;
    private Button likeBtn;

    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_tinder);
        briefDescription = (TextView) findViewById(R.id.briefDescription);
        briefDescription.setText("Loading...");
        imageView = (ImageView) findViewById(R.id.userPhoto);

        nopeBtn = (Button) findViewById(R.id.nopeBtn);
        nopeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("isiTinder", "Nope button clicked");
                sayNope();
                displayUser(currentUser);
            }
        });
        nopeBtn.setEnabled(false);

        likeBtn = (Button) findViewById(R.id.likeBtn);
        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("isiTinder", "Like button clicked");
                likeUser();
                displayUser(currentUser);
            }
        });
        likeBtn.setEnabled(false);

        getData();
    }

    public void getData (){
        dataManager = DataManager.getInstance(getApplicationContext());

        dataManager.getData(new IDataListener() {
            @Override
            public void onDataReceive(User user) {
                currentUser = user;
                displayUser(currentUser);
                likeBtn.setEnabled(true);
                nopeBtn.setEnabled(true);
            }

            @Override
            public void onError(String string) {

            }
        });
    }

    public void displayUser (User user){
        if (user != null) {
            Picasso.with(getApplicationContext()).load(user.getPicture().getMedium()).into(imageView);
            briefDescription.setText(user.getName().getFirst() + " " + user.getName().getLast());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), DetailActivity.class);
                    startActivity(i);
                }
            });
        }
        else{
            clearScreen();
            briefDescription.setText("No Data");
        }
    }

    public void clearScreen(){
        briefDescription.setText("");
        nopeBtn.setEnabled(false);
        likeBtn.setEnabled(false);
        imageView.setOnClickListener(null);
        imageView.setImageResource(android.R.color.transparent);
    }

    public void sayNope (){ currentUser = dataManager.getNext(); }


    public void likeUser (){
        dataManager.likeUser(currentUser);
        currentUser = dataManager.getNext();
    }

    @Override
    protected void onDestroy() {
        Log.d("isiTinder", "onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.d("isiTinder","onRestart");
        currentUser = dataManager.getCurrent();
        displayUser(currentUser);
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d("isiTinder","onStop");
        super.onStop();
    }

    @Override
    protected void onResume() {
        Log.d("isiTinder","onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d("isiTinder","onPause");
        super.onPause();
    }

    @Override
    protected void onStart() {
        Log.d("isiTinder","onStart");
        super.onStart();
    }

    public void nopeBtnClick (){

    }
}
