package model;

/**
 * Created by Sylvain on 26/01/2016.
 */
public class Result {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
