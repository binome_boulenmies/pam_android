package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sylvain on 26/01/2016.
 */
public class Results {

    private String nationality;
    private String seed;
    private String version;
    private List<Result> results;

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public void addResults(Results results) {this.results.addAll(results.getResults());}

    public void popUser (){
        results.remove(0);
    }

    public User topUser() {
        if (results.size() == 0)
            return null;

        return results.get(0).getUser();
    }

    public Results() {
        results = new ArrayList<Result>();
    }

}
